
package controlador;
import modelo.Recibo;
import vista.dlgRecibo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

//SE TERMINO EL PROGRAMA
public class Controlador implements ActionListener{
    
    private Recibo rec;
    private dlgRecibo vista;
    
    public Controlador(Recibo rec, dlgRecibo vista){
        this.rec = rec;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
    }
    
    private void iniciarVista(){
        vista.setTitle(" == Recibo == ");
        vista.setSize(650, 550);
        vista.setVisible(true);
    }
    
    public void limpiar(){
        vista.txtDomicilio.setText("");
        vista.txtNombre.setText("");
        vista.txtNumRec.setText("");
        vista.txtFecha.setText("");
        vista.txtKilowatts.setText("");
        vista.jblCPK.setText("0");
        vista.jblImp.setText("0");
        vista.jblSubTot.setText("0");
        vista.jblTotal.setText("0");
        vista.cbServicio.setSelectedIndex(0);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == vista.btnNuevo){
            vista.txtDomicilio.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtNumRec.setEnabled(true);
            vista.txtKilowatts.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.cbServicio.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource() == vista.btnGuardar){
            try{
                if(Integer.parseInt(vista.txtNumRec.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de recibo valido");
                }
                
                if(Float.parseFloat(vista.txtKilowatts.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de Kilowatts valido");
                }
                
                rec.setNumRecibo(Integer.parseInt(vista.txtNumRec.getText()));
                rec.setKilowattsConsum(Float.parseFloat(vista.txtKilowatts.getText()));
                rec.setNombre(vista.txtNombre.getText());
                rec.setFecha(vista.txtFecha.getText());
                rec.setDomicilio(vista.txtDomicilio.getText());
                rec.setCostoPK(Float.parseFloat(vista.jblCPK.getText()));
                rec.setTipoServicio(vista.cbServicio.getSelectedIndex());
                
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex2.getMessage());
                return;
            }
            
            limpiar();
        }
        
        if(e.getSource() == vista.btnMostrar){
            vista.txtNumRec.setText(Integer.toString(rec.getNumRecibo()));
            vista.txtNombre.setText(rec.getNombre());
            vista.txtFecha.setText(rec.getFecha());
            vista.txtDomicilio.setText(rec.getDomicilio());
            vista.txtKilowatts.setText(Float.toString(rec.getKilowattsConsum()));
            vista.jblCPK.setText(Float.toString(rec.getCostoPK()));
            vista.cbServicio.setSelectedIndex(rec.getTipoServicio());
            vista.jblSubTot.setText(Float.toString(rec.calcularSubt()));
            vista.jblImp.setText(Float.toString(rec.calcularImp()));
            vista.jblTotal.setText(Float.toString(rec.calcularTot()));
        }
        
        if(e.getSource() == vista.btnCancelar){
            limpiar();
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumRec.setEnabled(false);
            vista.txtKilowatts.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.cbServicio.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Esta seguro que desea salir?", "Cerrar", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        
    }

  
    public static void main(String[] args) {
        Recibo rec = new Recibo();
        dlgRecibo vista = new dlgRecibo(new JFrame(), true);
        
        Controlador contra = new Controlador(rec, vista);
        contra.iniciarVista();
    }

    
}

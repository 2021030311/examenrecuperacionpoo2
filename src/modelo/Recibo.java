
package modelo;

public class Recibo {
    int numRecibo, tipoServicio;
    float costoPK, kilowattsConsum;
    String nombre, domicilio, fecha;

    public Recibo() {
        numRecibo = 0;
        tipoServicio = -1;
        costoPK = 0;
        kilowattsConsum = 0;
        nombre = "";
        domicilio = "";
        fecha = "";
    }

    public Recibo(int numRecibo, int tipoServicio, float costoPK, float kilowattsConsum, String nombre, String domicilio, String fecha) {
        this.numRecibo = numRecibo;
        this.tipoServicio = tipoServicio;
        this.costoPK = costoPK;
        this.kilowattsConsum = kilowattsConsum;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fecha = fecha;
    }

    public Recibo(Recibo rec) {
        this.numRecibo = rec.numRecibo;
        this.tipoServicio = rec.tipoServicio;
        this.costoPK = rec.costoPK;
        this.kilowattsConsum = rec.kilowattsConsum;
        this.nombre = rec.nombre;
        this.domicilio = rec.domicilio;
        this.fecha = rec.fecha;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoPK() {
        return costoPK;
    }

    public void setCostoPK(float costoPK) {
        this.costoPK = costoPK;
    }

    public float getKilowattsConsum() {
        return kilowattsConsum;
    }

    public void setKilowattsConsum(float kilowattsConsum) {
        this.kilowattsConsum = kilowattsConsum;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    
    public String getFecha(){
        return fecha;
    }
    
    public void setFecha(String fecha){
        this.fecha = fecha;
    }
    
    public float calcularSubt(){
        if(this.tipoServicio == 0 ){
            costoPK=2;
            return costoPK * kilowattsConsum;
        }
        else if(this.tipoServicio == 1 ){
            costoPK=3;
            return costoPK * kilowattsConsum;
        }
        else if(this.tipoServicio == 2 ){
            costoPK=5;
            return costoPK * kilowattsConsum;
        }
        return 0;
    }
    
    public float calcularImp(){
        return calcularSubt() * (float)(0.16);
    }
    
    public float calcularTot(){
        return calcularSubt() * (float)(1.16);
    }
}
